import pytest

import csv_parse


def test_parser_is_not_implemented():
    '''Test that the csv parser is not implemented'''

    with pytest.raises(NotImplementedError):
        csv_parse.parse_csv('whatever_filename.csv')
